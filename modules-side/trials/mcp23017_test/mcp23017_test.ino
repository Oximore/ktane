/* Exemple toggle.pde
   Utilise la librairie Adafruit-MCP23017 pour changer une sortie du MCP23017. 
   
   Code écrit par LadyAda pour AdaFruit Industries [www.adafruit.com], Domaine Publique
   
   TRADUCTION FRANCAISE par Meurisse D. pour MCHobby.be [www.mchobby.be], CC-BY-SA pour tâche de traduction.
   COMPLEMENT DU CODE   par Meurisse D. pour MCHobby.be [www.mchobby.be], Domaine publique... comme AdaFruit
   TUTORIEL complémentaire EN FRANCAIS par MCHobby.be sur (voir wiki pour licence tutoriel)
      http://mchobby.be/wiki/index.php?title=MCP23017

   Acheter un MCP23017
      http://shop.mchobby.be/product.php?id_product=218
*/
   
#include <Wire.h>
#include "Adafruit_MCP23017.h"

// Test de base d'ecriture d'état pour un "MCP23017 I/O expander"
// Domaine publique (code d'origine)!

// Connectez la broche #12 du MCP23017 sur Arduino broche Analogique A5 (Horloge I2C, SCL)
// Connectez la broche #13 du MCP23017 sur Arduino broche Analogique A4 (Données I2C, SDA)
// Connectez la broche #15, 16 et 17 du MCP23017 sur DNG (Sélection de l'adresse)
// Connectez la broche #9 du MCP23017 sur 5V (Alimentation)
// Connectez la broche #10 du MCP23017 sur GND (Masse commune)

// Sortie #0 (GPA0) est sur la Broche 21 du MCP, vous pouvez y connecter une LED 
// ou autre choses (lui même raccordé vers la masse/GND)

// Ajout MCHobby:
// - Connectez la broche #18 du MCP23017 sur 5V (désactiver la ligne Reset)
// - Activer la broche 13 pour faire clignoter la LED sur la carte Arduino au même rythme
//   (sert de témoin d'exécution et permet de détecter les erreurs de montage.
// - Utiliser un délai de pause de 1 seconde au lieu de 100ms
//
Adafruit_MCP23017 mcp;
  
void setup() {
  //mcp.begin(); // Utilise l'adresse par défaut qui est 0
  pinMode( 13, OUTPUT );
  //mcp.pinMode(0, OUTPUT); 
}


// Activer/désactiver la sortie #0 du MCP toutes les 100 millisecondes

void loop() {
  digitalWrite( 13, HIGH);   // Activer LED témoin sur Arduino
  //mcp.digitalWrite(0, HIGH); // Activer sortie du MCP
  delay(500); // Attendre 1000ms
 
  digitalWrite( 13, LOW);  // Désactiver LED témoin sur Arduino
  //mcp.digitalWrite(0, LOW); // Désactiver la sortie)
  delay(500); // Attendre 1s (1000ms)
}

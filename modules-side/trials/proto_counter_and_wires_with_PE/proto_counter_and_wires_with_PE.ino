#include <SevSeg.h>
#include <PortExpander.h>

namespace PortExpander {

  enum Pin: uint8_t {
    ClockD1 = 2, ClockD2 = 3, ClockD3 = 4, ClockD4 = 5,
      ClockA = 6, ClockB = 7, ClockC = 8, ClockD = 9, ClockE = 10, ClockF = 11, ClockG = 12, ClockDP = 13,
      
      StateLedBlue  = vpin(0), StateLedGreen = vpin(1), StateLedRed = vpin(2),
      ErrorLed1 = vpin(3), ErrorLed2 = vpin(4), ErrorLed3 = vpin(5),
      Wires1 = vpin(8), Wires2 = vpin(9), Wires3 = vpin(10),
      Wires4 = vpin(11), Wires5 = vpin(12), Wires6 = vpin(13)
      };
  

  /*********************/
  /*** COUNTER STATE ***/
  /*********************/

  enum CounterState{Powered, Running, Waiting};
  CounterState currentCounterState;

  void initCounter();
  void setCounterState(CounterState cs);
  void turnOnLedCounterState();
  
  void initCounter() {
    pinMode(Pin::StateLedRed,   OUTPUT);
    pinMode(Pin::StateLedGreen, OUTPUT);
    pinMode(Pin::StateLedBlue,  OUTPUT);

    setCounterState(CounterState::Powered);
  }

  void turnOnLedCounterState() {
    digitalWrite(Pin::StateLedRed,   LOW);
    digitalWrite(Pin::StateLedGreen, LOW);
    digitalWrite(Pin::StateLedBlue,  LOW);

    switch(currentCounterState) {
    case CounterState::Powered :
      // Red
      digitalWrite(Pin::StateLedRed, HIGH);
      break;
    case CounterState::Waiting :
      // Orange
      digitalWrite(Pin::StateLedRed,   HIGH);
      digitalWrite(Pin::StateLedGreen, HIGH);
      break;
    case CounterState::Running :
      // Green
      digitalWrite(Pin::StateLedGreen, HIGH);
      break;
    default :
      // Blue
      digitalWrite(Pin::StateLedBlue, HIGH);
    }
  }

  void setCounterState(CounterState cs) {
    currentCounterState = cs;
    turnOnLedCounterState();
  }



  /*******************/
  /*** ERROR STATE ***/
  /*******************/

  int8_t errorNumber = 0;

  void initErrorLed();
  void setError(uint8_t errorNb);
  void addError(int8_t error_nb);
  void removeError(int8_t error_nb);
  void displayError();
  int maxError = 3;
  
  void initErrorLed() {
    pinMode(Pin::ErrorLed1, OUTPUT);
    pinMode(Pin::ErrorLed2, OUTPUT);
    pinMode(Pin::ErrorLed3, OUTPUT);

    displayError();
  }

  void setError(uint8_t errorNb) {
    if (0 < errorNb  || errorNb <= maxError) {
      errorNumber = errorNb;
    }
    displayError();
  }

  void explode();
  
  void addError(int8_t error_nb = 1) {
    errorNumber += error_nb;
    displayError();
    if (errorNumber >= maxError) {
      explode();
    }
  }

  void removeError(int8_t error_nb = 1) {
    errorNumber -= error_nb;
    displayError();
  }

  void displayError() {
    uint8_t errorNb = errorNumber;
    if (errorNb < 0 || errorNb > maxError) {
      errorNb = 0;
    }

    digitalWrite(Pin::ErrorLed1, LOW);
    digitalWrite(Pin::ErrorLed2, LOW);
    digitalWrite(Pin::ErrorLed3, LOW);
  
    if (errorNb > 0) {
      digitalWrite(Pin::ErrorLed1, HIGH);
      if (errorNb > 1) {
      	digitalWrite(Pin::ErrorLed2, HIGH);
	      if (errorNb > 2) {
	        digitalWrite(Pin::ErrorLed3, HIGH);
      	}
      }
    }
  }


  /*******************/
  /*** CLOCK STATE ***/
  /*******************/

  enum StateClock {RunningClock, PausedClock};

  // time of the timer (ms)
  unsigned long TotalTimerTime = 0;
  // millis() when timer start (ms)
  unsigned long StartTimerDate = 0;
  // millis() when timer stop (ms)
  unsigned long StopTimerDate = 0;

  // Current Status of the timer
  StateClock ClockStatus;

  SevSeg Clock;

  void initClock();
  void setClock(unsigned long secondes);
  void startClock();
  void stopClock();
  void resetClock();
  void displayClock();
  

  void initClock() {
    byte numDigits = 4;
    byte digitPins[] = {Pin::ClockD1, Pin::ClockD2, Pin::ClockD3, Pin::ClockD4};
    byte segmentPins[] = {Pin::ClockA, Pin::ClockB, Pin::ClockC, Pin::ClockD, Pin::ClockE, Pin::ClockF, Pin::ClockG, Pin::ClockDP};
    bool resistorsOnSegments = false; // 'false' means resistors are on digit pins
    byte hardwareConfig = COMMON_CATHODE; // See README.md for options
    bool updateWithDelays = false; // Default. Recommended
    bool leadingZeros = false; // Use 'true' if you'd like to keep the leading zeros

    Clock.begin(hardwareConfig, numDigits, digitPins, segmentPins, resistorsOnSegments, updateWithDelays, leadingZeros);
    Clock.setBrightness(90);

    ClockStatus = StateClock::PausedClock;
    setClock(60);
  }

  void setClock(unsigned long secondes) {
    ClockStatus = StateClock::PausedClock;
    TotalTimerTime = 1000 * secondes;
  }

  void startClock() {
    StartTimerDate = millis();
    StopTimerDate = StartTimerDate + TotalTimerTime;
    ClockStatus = StateClock::RunningClock;
  }

  void stopClock() {
    ClockStatus = StateClock::PausedClock;
  }

  // same as setClock() for now
  void resetClock() {
    StartTimerDate = millis();
    StopTimerDate = StartTimerDate + TotalTimerTime;
    ClockStatus = StateClock::RunningClock;
  }

  static unsigned long lastTimePrinted = 0;

  boolean hasExploded = false;
  
  void explode(){
    if (hasExploded == false) {
      hasExploded = true;
      stopClock();
      //Clock.setNumber(8004);
      char boom[] = "BOOM";
      Clock.setChars(boom);
    }
  }

  void displayClock() {
    unsigned long nowTime = millis();

    switch (ClockStatus) {
    case StateClock::RunningClock :
      if (StopTimerDate > nowTime) {
        unsigned long remainingTime = StopTimerDate - nowTime; // ms
	      Clock.setNumber((int)(remainingTime/1000));
      }
      else {
        // exploding
        explode();
      }
      break;
    case StateClock::PausedClock :
      break;
    default:
      Clock.setNumber((int)TotalTimerTime);
    }

    Clock.refreshDisplay();
  }


  /********************/
  /*** SIMPLE WIRES ***/
  /********************/

  int wireNb = 0;
  int correctWire = 0;
  uint8_t* wiresPin;
  boolean* wasUsedWires;

  void initWires();
  void resetWires();
  void wiresOk();
  void wiresBad();
  void checkWires();
  
  void initWires() {
    randomSeed(analogRead(0));
    
    wireNb = 6;
    correctWire = random(wireNb);
    
    wiresPin = new uint8_t[wireNb];
    wasUsedWires = new boolean[wireNb];
    
    for (int iwire = 0 ; iwire < wireNb ; ++iwire) {
      wiresPin[iwire] = Pin::Wires1 + iwire;
    }
   
    // INPUT_PULLUP enables the Arduino Internal Pull-Up Resistor
    for (int iwire = 0 ; iwire < wireNb ; ++iwire) {
      //pinMode(wiresPin[iwire], INPUT_PULLUP);
      pinMode(wiresPin[iwire], INPUT);
      pullUp(wiresPin[iwire], HIGH);
    }

    resetWires();
  }


  void resetWires() {
    for (int iwire = 0 ; iwire < wireNb ; ++iwire) {
      wasUsedWires[iwire] = false;
    }
  }


  void wiresOk() {
    //Serial.println("Bombe désamorcée !");
    //Clock.setNumber(8888, 0);
    stopClock();
  }
 
  void wiresBad() {
    addError();
  }

  void checkWires() {
    bool buttonState;
    for (int iwire = 0 ; iwire < wireNb ; ++iwire) {
      buttonState = digitalRead(wiresPin[iwire]);
      if (!wasUsedWires[iwire] && iwire == correctWire && buttonState == HIGH) {
	wasUsedWires[iwire] = true;
	wiresOk();
      }
      else if (!wasUsedWires[iwire] && iwire != correctWire && buttonState == HIGH) {
	wasUsedWires[iwire] = true;
	wiresBad();
      }
    }
  }



  /************/
  /*** MAIN ***/
  /************/

  void setup() {
    //delay(1*1000);

    setupPortExpander();
    initCounter();
    initClock();
    initErrorLed();
    initWires();
    //initInput();
    
    setCounterState(CounterState::Running);
    startClock();
  }

  void loop() {
    displayClock();
    checkWires();
    //readInput();
  }

} // namespace PortExpander

void setup() {
  PortExpander::setup();
}

void loop() {
  PortExpander::loop();
}



  /*************/
  /*** INPUT ***/
  /*************/

  /*
  void initInput() {
    // opens serial port, sets data rate to 9600 bps
    Serial.begin(9600);
  }

  void readInput() {
    if (Serial.available() > 0) {
      int incomingByte = 0;
      // read the incoming byte:
      incomingByte = Serial.read();

#ifdef NDEBUG
      // say what you got:
      Serial.print("I received: ");
      Serial.println(incomingByte, DEC);
#endif // NDEBUG

      if ('0' < incomingByte && incomingByte <= '9') {
  unsigned int newTime = (incomingByte - '0') * 60;
  setClock(newTime);
      }

      switch (incomingByte) {
      case '+':
        addError();
        break;
      case '-':
        removeError();
        break;
      case 'a':
        startClock();
        break;
      case 'z':
        stopClock();
        break;
      case 'r':
        resetWires();
        setError(0);
        startClock();
        hasExploded = false;
        break;
      }
    }
  }
  //*/

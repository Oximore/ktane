/**
 * MCP23017_PortExpander.h - Library to help with use of MCP23017 I2C port expander
 * Created by Lux Benjamin, dec 13, 2017
 **/


#ifndef MCP23017_PORTEXPANDER_HELPER
#define MCP23017_PORTEXPANDER_HELPER

#include "Arduino.h"
#include <Wire.h>
#include <Adafruit_MCP23017.h>

/**
 * Help with Shift Regisiter (SR) SN74HC595 as an output
 * After initialisation, SR uses become transparent
 * It emulates virtual pin which are grether than 20 (20 pin for Arduino Uno)
 *
 * This is not appropriate for particular use of SR, like shifting value one by one.
 * For exemple shifting a light led.
 * 
 * In Arduino core function pin type is uint8_t. uint8_t (0 to 255).
 * So if we want to keep arduino prototypes we can't use more than 255 pin.
 *
 *
 * from SN74HC595 Data sheet 
 * at https://assets.nexperia.com/documents/data-sheet/74HC_HCT595.pdf
 *       ____ ____
 *      |    U    |
 * Q1   |1      16| Vcc*   supply voltage (+5 V)
 * Q2   |2      15| Q0     parallel data output 0
 * Q3   |3      14| DS*    serial data input
 * Q4   |4      13| OE**   output enable input (active LOW)
 * Q5   |5      12| STCP*  storage register clock input
 * Q6   |6      11| SHCP*  shift register clock input
 * Q7   |7      10| MR**   master reset (active LOW)
 * GND* |8       9| Q7S    serial data output
 *      |_________|
 * 
 * Q0 to Q8  parallel data output 0
 * GND       ground (0 V)
 *
 * *: this pin have to be connect to the Arduino
 * **: you may want to connect these pin to :
 *   MR : connect to HIGH
 *   OE : connect to LOW
 **/



/**
 * Arduino pin function are :
 *
 * // Digital I/O
 * void pinMode(uint8_t pin, uint8_t mode);
 * void digitalWrite(uint8_t pin, uint8_t val);
 * int digitalRead(uint8_t pin);
 % *
 * // Analog I/O
 * void analogReference(uint8_t mode);
 * int analogRead(uint8_t pin);
 * void analogWrite(uint8_t pin, int val); // - PWM
 *
 * // Due & Zero only
 * analogReadResolution()
 * analogWriteResolution()
 *
 * Here we wrap only the 3 first
 **/

/**
 * Usage :
 * decalre your physical distribution with this function :
 * ShiftRegisterHelper::ShiftRegister::setupShiftRegister(uint8_t ds_pin, uint8_t shcp_pin, uint8_t stcp_pin, int sr_num = 1)
 *
 * then you can use these functions as there is more pin :
 *
 * void vPinMode(uint8_t pin, uint8_t mode);
 * void vDigitalWrite(uint8_t pin, uint8_t val);
 * int  vDigitalRead(uint8_t pin);
 *
 * Virtual pin count starts at ShiftRegisterHelper::emulated_pin_min and end at emulated_pin_max (excluded).
 * Numbering depends on how you initialize your ShiftRegisters.
 **/

/**
 * Todo :
 * Documentation, clear code, comments and library.
 **/


namespace MCP23017_PortExpanderHelper {

  void pinMode(uint8_t pin, uint8_t mode);
  void digitalWrite(uint8_t pin, uint8_t val);
  int  digitalRead(uint8_t pin);

  /**
   * class MCP23017_PortExpander
   *
   * Only implemented as an output (for now)
   **/

  class MCP23017_PortExpander {
  public:
    static void setupPortExpander(int address = 0);

    static uint8_t emulatedPinMin();
    static uint8_t emulatedPinMax();

    friend void pinMode(uint8_t pin, uint8_t mode);
    friend void digitalWrite(uint8_t pin, uint8_t val);
    friend int  digitalRead(uint8_t pin);

  private:
    // max pin + 1 on the Arduino Uno
    // min pin which can be emulated
    static uint8_t _emulated_pin_min; // virtual pins start at emulated_pin_min, fix on a run
    static uint8_t _emulated_pin_max; // virtual pins ends  at emulated_pin_max-1, change in a run

    const static uint8_t _pin_nb; //out_pin_nb_by_pe;
    const static uint8_t _scl_pin;
    const static uint8_t _sda_pin;

    Adafruit_MCP23017 _mcp;
    uint8_t  _first_pin;
    uint8_t  _last_pin;

    // Vector of all PE declared
    static MCP23017_PortExpander* _portExpanderPtrVect[50]; // todo : not more than 50 PE ...
    static int _pePtrVectCmpt;

    MCP23017_PortExpander(int address);
    ~MCP23017_PortExpander();

    static MCP23017_PortExpander* _getMCP23017_PortExpander(uint8_t pin);

    void _vPinMode(uint8_t pin, uint8_t mode);
    void _vDigitalWrite(uint8_t pin, uint8_t val);
    int  _vDigitalRead(uint8_t pin);
    boolean _containsPin(uint8_t pin) const;

  }; // class MCP23017_PortExpander


} // MCP23017_PortExpanderHelper

#endif // MCP23017_PORTEXPANDER_HELPER


namespace MCP23017_PortExpanderHelper {

  // MCP23017 have 16 output for each chip
  const uint8_t MCP23017_PortExpander::_pin_nb = 16; //out_pin_nb_by_pe = 16;

  const uint8_t MCP23017_PortExpander::_scl_pin = 12; // 12: I2C clock
  const uint8_t MCP23017_PortExpander::_sda_pin = 13; // 13: I2C data

  MCP23017_PortExpander* MCP23017_PortExpander::_portExpanderPtrVect[50];
  // todo : max with 3 bit address ...
  int MCP23017_PortExpander::_pePtrVectCmpt = 0;

  // Atmega328 have like 20 pin (?)
  uint8_t MCP23017_PortExpander::_emulated_pin_min = 40;
  uint8_t MCP23017_PortExpander::_emulated_pin_max = 40;


  MCP23017_PortExpander::MCP23017_PortExpander(int address) {
    _mcp.begin(address);

    uint8_t first_pin = _emulated_pin_min;

    if (_pePtrVectCmpt != 0) {
      first_pin = _portExpanderPtrVect[_pePtrVectCmpt-1]->_last_pin + 1;
    }

    if (first_pin >= _emulated_pin_min) {
      _first_pin = first_pin;
      _last_pin = _first_pin + _pin_nb - 1;
      _emulated_pin_max += _pin_nb;
    }

  }

  MCP23017_PortExpander::~MCP23017_PortExpander() {
  }


  void MCP23017_PortExpander::setupPortExpander(int address) {
    _portExpanderPtrVect[_pePtrVectCmpt++] = new MCP23017_PortExpander(address);
  }

  MCP23017_PortExpander* MCP23017_PortExpander::_getMCP23017_PortExpander(uint8_t pin) {
    for (int ipePtr = 0 ; ipePtr < _pePtrVectCmpt ; ++ipePtr) {
      if ( _portExpanderPtrVect[ipePtr]->_containsPin(pin) ) {
  return _portExpanderPtrVect[ipePtr];
      }
    }

    return NULL;
  }


  void MCP23017_PortExpander::_vPinMode(uint8_t pin, uint8_t mode) {
    _mcp.pinMode(pin-_first_pin, mode);
  }

  void MCP23017_PortExpander::_vDigitalWrite(uint8_t pin, uint8_t val) {
    _mcp.digitalWrite(pin-_first_pin, val);
  }

  int MCP23017_PortExpander::_vDigitalRead(uint8_t pin) {
    return _mcp.digitalRead(pin-_first_pin);
  }

  boolean MCP23017_PortExpander::_containsPin(uint8_t pin) const {
    return _first_pin <= pin && pin <= _last_pin;
  }

  uint8_t MCP23017_PortExpander::emulatedPinMin() {
    return _emulated_pin_min;
  }

  uint8_t MCP23017_PortExpander::emulatedPinMax() {
    return _emulated_pin_max;
  }


  /**
   * Bad naming
   * todo : static ?
   **/
  inline boolean isEmulated(uint8_t pin) {
    return pin >= MCP23017_PortExpander::emulatedPinMin();
  }


  /**
   * Arduino wrapping function
   **/

  // Digital I/O

  void pinMode(uint8_t pin, uint8_t mode) {
    if ( isEmulated(pin) ) {
      MCP23017_PortExpander::_getMCP23017_PortExpander(pin)->_vPinMode(pin, mode);
    }
    else {
      // call Arduino pinMode
      ::pinMode(pin,mode);
    }
  }

  void digitalWrite(uint8_t pin, uint8_t val) {
    if ( isEmulated(pin) ) {
      MCP23017_PortExpander::_getMCP23017_PortExpander(pin)->_vDigitalWrite(pin, val);
    }
    else {
      // call Arduino digitalWrite
      ::digitalWrite(pin, val);
    }
  }

  int digitalRead(uint8_t pin) {
    if ( isEmulated(pin) ) {
      return MCP23017_PortExpander::_getMCP23017_PortExpander(pin)->_vDigitalRead(pin);
    }
    else {
      // call Arduino digitalRead
      return ::digitalRead(pin);
    }
  }

} // namespace MCP23017_PortExpanderHelper


int pin = 0;

namespace MCP23017_PortExpanderHelper {
   
  void setup() {
    MCP23017_PortExpander::setupPortExpander(0);    
    // We can now use virtual pin from ShiftRegister::emulatedPinMin() to ShiftRegister::emulatedPinMax() (excluded)
    pin = MCP23017_PortExpander::emulatedPinMin();
    pinMode(pin, OUTPUT);
  }

  
  void loop() {
    digitalWrite(pin, HIGH);
    delay(500);
  
    digitalWrite(pin, LOW);
    delay(500);
  }

} // namespace MCP23017_PortExpanderHelper

void setup() {
  MCP23017_PortExpanderHelper::setup();
}

void loop() {
  MCP23017_PortExpanderHelper::loop();
}



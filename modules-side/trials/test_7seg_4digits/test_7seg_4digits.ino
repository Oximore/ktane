
#include <ShiftRegister.h>
#include <SevSeg.h>
  
// #define VPIN(a) (SR_VPIN_START + a)

/**
 *            C1   A    F    C2   C3   B
 *  __________|____|____|____|____|____|___________
 * |                                               |
 * |    __a__      __a__      __a__      __a__     |
 * |   |     |    |     |    |     |    |     |    |
 * |  f|     |b  f|     |b  f|     |b  f|     |b   |
 * |   |__g__|    |__g__|    |__g__|    |__g__|    |
 * |   |     |    |     |    |     |    |     |    |
 * |  e|     |c  e|     |c  e|     |c  e|     |c   |
 * |   |__d__| .h |__d__| .h |__d__| .h |__d__| .h |
 * |                                               |
 * |      C1         C2         C3         C4      |
 * |_______________________________________________|
 *            |    |    |    |    |    |
 *            E    D    H    C    G    C4
 **/

// Todo utiliser les lib déjà faites


namespace ShiftRegister {

  enum Pin: uint8_t {StateLedRed = 3, StateLedGreen = 5, StateLedBlue = 6,
                   SR1_ds = 8, SR1_stcp = 10, SR1_shcp = 9,
                   A = vpin(3), B = vpin(4), C = vpin(5), D = vpin(6), E = vpin(7), F = vpin(8), G = vpin(9), H = vpin(10),
                   Digit1 = vpin(11), Digit2 = vpin(12), Digit3 = vpin(13), Digit4 = vpin(14),
                   ErrorLed1 = vpin(0), ErrorLed2 = vpin(1), ErrorLed3 = vpin(2)
                   /*Wires1, Wires2, Wires3*/
                   };


  /************************************************/

  /************ 7 Seg Display *****************/

  int ss_high = HIGH; // revoir
  int ss_low = !ss_high;  

  byte seven_seg_digits[10][7] = { { 1,1,1,1,1,1,0 },  // = 0
                                 { 0,1,1,0,0,0,0 },  // = 1
                                 { 1,1,0,1,1,0,1 },  // = 2
                                 { 1,1,1,1,0,0,1 },  // = 3
                                 { 0,1,1,0,0,1,1 },  // = 4
                                 { 1,0,1,1,0,1,1 },  // = 5
                                 { 1,0,1,1,1,1,1 },  // = 6
                                 { 1,1,1,0,0,0,0 },  // = 7
                                 { 1,1,1,1,1,1,1 },  // = 8
                                 { 1,1,1,0,0,1,1 }   // = 9
                                 };

  void writeDot(byte dot) {
    ShiftRegister::digitalWrite(Pin::H, dot);
  }
   
  void writeDigit(byte digit) {
    byte pin = Pin::A;
    for (byte segCount = 0; segCount < 7; ++segCount) {
     ShiftRegister::digitalWrite(pin, seven_seg_digits[digit][segCount]);
     ++pin; // Work if Pin A to G are folowing
    }
  }


  void turnDigit(byte digit, byte value) {
    ShiftRegister::digitalWrite(Pin::Digit1 + digit - 1 , !value);
  }
  
  void turnDigit(byte value) {
    for (int i = 1 ; i <= 4 ; ++i ) {
       turnDigit(i, value);
    }
  }


  
  void writeDot(byte dot, byte place) {
    turnDigit(LOW);
    turnDigit(place, HIGH);
    writeDot(dot);
  }
  
  void writeDigit(byte digit, byte place) {
    turnDigit(LOW);
    turnDigit(place, HIGH);
    writeDigit(digit);
  }

  /*************************************************/


    
  /************* SETUP **********************/
  
  SevSeg sevSeg;
  
  void setup() {
    ShiftRegister::pinMode(Pin::StateLedRed,   OUTPUT);
    ShiftRegister::pinMode(Pin::StateLedGreen, OUTPUT);
    ShiftRegister::pinMode(Pin::StateLedBlue,  OUTPUT);

    int sr_num = 2;

    // We tell the program than we have a shift register connected to pin 9, 10 and 11.
    setupShiftRegister(Pin::SR1_ds, Pin::SR1_shcp, Pin::SR1_stcp, sr_num);    
    // We can now use virtual pin from ShiftRegister::emulatedPinMin() to ShiftRegister::emulatedPinMax() (excluded)

    // *
    uint8_t high_value = 10; // 0 to 255
    analogWrite(Pin::StateLedRed, high_value);
    delay(200);
    analogWrite(Pin::StateLedBlue, high_value);
    analogWrite(Pin::StateLedRed, 0);
    delay(200);
    analogWrite(Pin::StateLedGreen, high_value);
    analogWrite(Pin::StateLedBlue, 0);
    // */

    sevSeg.Begin(0, Pin::Digit1, Pin::Digit2, Pin::Digit3, Pin::Digit4, Pin::A, Pin::B, Pin::C, Pin::D, Pin::E, Pin::F, Pin::G, Pin::H);
    }

  /************ Error Led *****************/

  void loop_test_error_led() {
    static int counter = 0;
    int min_pin = Pin::ErrorLed1;
    int max_pin = Pin::ErrorLed3;
    
    int lenght = max_pin - min_pin + 1;

    // digitalWrite is called on a virtual pin.
    // this mean that is not a real pin of the Atmega but a pin of your shift register
    digitalWrite(min_pin + (counter%lenght), (counter % (lenght*2) < lenght) ? HIGH : LOW);
    counter++;
    delay(300);
  }



  void loop_test_7seg() {
    
    for ( int idigitPin = Pin::Digit1 ; idigitPin <= Pin::Digit4 ; idigitPin++ ) {
      digitalWrite(idigitPin, ss_low);
    }
    
    for (int ipin = Pin::A ; ipin <= Pin::H ; ipin++) {
      digitalWrite(ipin, ss_high);
      delay(100);
      digitalWrite(ipin, ss_low);
    }

    for (int place = 1 ; place <= 4 ; place++) {
      for (int i = 0 ; i < 10 ; i++) {
        writeDigit(i, place);
        delay(1000);
      }
    }
 
  }

  byte counter = 0;
  
  void loop_test_7seg_lib() {
    sevSeg.NewNum(42, 2);
    for (int i = 0 ; i < 1000 ; i++) {
      sevSeg.PrintOutput();
      //delayMicroseconds(1);
    }
    counter++;
  }
  

} // namespace ShiftRegisterHelper

void setup() {
  ShiftRegister::setup();
}

void loop() {
  //ShiftRegister::loop_test_error_led();
  // ShiftRegister::loop_test_7seg();
  ShiftRegister::loop_test_7seg_lib();
}


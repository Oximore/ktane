#include <SevSeg.h>
#include <PortExpander.h>

namespace PortExpander {
  
  class DemoCounterAndWires { 
  private:

    
    /***********/
    /*** PIN ***/
    /***********/
    
    enum /*class*/ Pin: uint8_t {
      ClockD1 = 2, ClockD2 = 3, ClockD3 = 4, ClockD4 = 5,
      ClockA = 6, ClockB = 7, ClockC = 8, ClockD = 9, ClockE = 10, ClockF = 11, ClockG = 12, ClockDP = 13,
      
      StateLedBlue  = vpin(0), StateLedGreen = vpin(1), StateLedRed = vpin(2),
      ErrorLed1 = vpin(3), ErrorLed2 = vpin(4), ErrorLed3 = vpin(5),
      Wires1 = vpin(8), Wires2 = vpin(9), Wires3 = vpin(10),
      Wires4 = vpin(11), Wires5 = vpin(12), Wires6 = vpin(13)
      };
  

    /*********************/
    /*** COUNTER STATE ***/
    /*********************/

    class CounterState {
    public:
      enum class State{Powered, Running, Waiting};
    private:
      State _currentState;

    public:
      void init() {
        pinMode(Pin::StateLedRed,   OUTPUT);
        pinMode(Pin::StateLedGreen, OUTPUT);
        pinMode(Pin::StateLedBlue,  OUTPUT);

        setState(State::Powered);
      }
  
      void setState(State cs) {
        _currentState = cs;
        turnOnLed();
      }
    
    private:
      void turnOnLed() {
        digitalWrite(Pin::StateLedRed,   LOW);
        digitalWrite(Pin::StateLedGreen, LOW);
        digitalWrite(Pin::StateLedBlue,  LOW);
    
        switch(_currentState) {
        case State::Powered :
          // Red
          digitalWrite(Pin::StateLedRed, HIGH);
          break;
        case State::Waiting :
          // Orange
          digitalWrite(Pin::StateLedRed,   HIGH);
          digitalWrite(Pin::StateLedGreen, HIGH);
          break;
        case State::Running :
          // Green
          digitalWrite(Pin::StateLedGreen, HIGH);
          break;
        default :
          // Blue
          digitalWrite(Pin::StateLedBlue, HIGH);
        }
      }
    }; // class CounterState

    
    /*******************/
    /*** CLOCK STATE ***/
    /*******************/

    class ClockState {
    public:
      enum class State {RunningClock, PausedClock};
  
    private:
      unsigned long _totalTimerTime = 0;  // time of the timer (ms)
      unsigned long _startTimerDate = 0;  // millis() when timer start (ms)
      unsigned long _stopTimerDate = 0;   // millis() when timer stop (ms)

      // Current Status of the timer
      State _state;
      SevSeg _sevSegClock;

      // static unsigned long lastTimePrinted = 0;
      boolean _hasExploded = false;
      

    public:
      void init() {
        byte numDigits = 4;
        byte digitPins[] = {Pin::ClockD1, Pin::ClockD2, Pin::ClockD3, Pin::ClockD4};
        byte segmentPins[] = {Pin::ClockA, Pin::ClockB, Pin::ClockC, Pin::ClockD, Pin::ClockE, Pin::ClockF, Pin::ClockG, Pin::ClockDP};
        bool resistorsOnSegments = false; // 'false' means resistors are on digit pins
        byte hardwareConfig = COMMON_CATHODE; // See README.md for options
        bool updateWithDelays = false; // Default. Recommended
        bool leadingZeros = false; // Use 'true' if you'd like to keep the leading zeros
    
        _sevSegClock.begin(hardwareConfig, numDigits, digitPins, segmentPins, resistorsOnSegments, updateWithDelays, leadingZeros);
        _sevSegClock.setBrightness(90);
    
        _state = State::PausedClock;
        setClock(60);
      }
    
      void setClock(unsigned long secondes) {
        _state = State::PausedClock;
        _totalTimerTime = 1000 * secondes;
      }
    
      void start() {
        _startTimerDate = millis();
        _stopTimerDate = _startTimerDate + _totalTimerTime;
        _state = State::RunningClock;
      }
    
      void stop() {
        _state = State::PausedClock;
      }
    
      // same as setClock() for now
      void resetClock() {
        _startTimerDate = millis();
        _stopTimerDate  = _startTimerDate + _totalTimerTime;
        _state = State::RunningClock;
      }
    
      void explode(){
        if (_hasExploded == false) {
          _hasExploded = true;
          stop();
          //_sevSegClock.setNumber(8004);
          char boom[] = "BOON";
          _sevSegClock.setChars(boom);
        }
      }
    
      void displayClock() {
        unsigned long nowTime = millis();
    
        switch (_state) {
        case State::RunningClock :
          if (_stopTimerDate > nowTime) {
            unsigned long remainingTime = _stopTimerDate - nowTime; // ms
    	      _sevSegClock.setNumber((int)(remainingTime/1000));
          }
          else {
            // exploding
            explode();
          }
          break;
        case State::PausedClock :
          break;
        default:
          _sevSegClock.setNumber((int)_totalTimerTime);
        }
    
        _sevSegClock.refreshDisplay();
      }
    }; // class ClockState
  
    /*******************/
    /*** ERROR STATE ***/
    /*******************/

    class ErrorState {
    private:
      int8_t _errorNumber = 0;
      int _maxError = 3;

      ClockState* _cs = NULL;
      
    public:
      ErrorState(ClockState& cs) {
        _cs = &cs;
      }
      
      void init() {
        pinMode(Pin::ErrorLed1, OUTPUT);
        pinMode(Pin::ErrorLed2, OUTPUT);
        pinMode(Pin::ErrorLed3, OUTPUT);
    
        displayError();
      }
    
      void setError(uint8_t errorNb) {
        if (0 < errorNb  || errorNb <= _maxError) {
          _errorNumber = errorNb;
        }
        displayError();
      }
    
      void addError(int8_t error_nb = 1) {
        _errorNumber += error_nb;
        displayError();
        if (_errorNumber >= _maxError) {
          _cs->explode();
        }
      }
    
      void removeError(int8_t error_nb = 1) {
        _errorNumber -= error_nb;
        displayError();
      }
    
    private:
      void displayError() {
        uint8_t errorNb = _errorNumber;
        if (errorNb < 0 || errorNb > _maxError) {
          errorNb = 0;
        }
    
        digitalWrite(Pin::ErrorLed1, LOW);
        digitalWrite(Pin::ErrorLed2, LOW);
        digitalWrite(Pin::ErrorLed3, LOW);
      
        if (errorNb > 0) {
          digitalWrite(Pin::ErrorLed1, HIGH);
          if (errorNb > 1) {
            digitalWrite(Pin::ErrorLed2, HIGH);
            if (errorNb > 2) {
              digitalWrite(Pin::ErrorLed3, HIGH);
            }
          }
        }
      }
    }; // class ErrorState
  
    /********************/
    /*** SIMPLE WIRES ***/
    /********************/

    class SimpleWires {
    private:
      int _wireNb = 0;
      int _correctWire = 0;
      uint8_t* _wiresPin;
      boolean* _wasUsedWires;

      ClockState* _cs = NULL;
      ErrorState* _es = NULL;
    public:
      SimpleWires(ClockState& cs, ErrorState& es) {
        _cs = &cs;
        _es = &es;
      }

      void init() {
        randomSeed(analogRead(0));
        
        _wireNb = 6;
        _correctWire = random(_wireNb);
        
        _wiresPin = new uint8_t[_wireNb];
        _wasUsedWires = new boolean[_wireNb];
        
        for (int iwire = 0 ; iwire < _wireNb ; ++iwire) {
          _wiresPin[iwire] = Pin::Wires1 + iwire;
        }
       
        // INPUT_PULLUP enables the Arduino Internal Pull-Up Resistor
        for (int iwire = 0 ; iwire < _wireNb ; ++iwire) {
          //pinMode(_wiresPin[iwire], INPUT_PULLUP);
          pinMode(_wiresPin[iwire], INPUT);
          pullUp(_wiresPin[iwire], HIGH);
        }
    
        resetWires();
      }

      void checkWires() {
        bool buttonState;
        for (int iwire = 0 ; iwire < _wireNb ; ++iwire) {
          buttonState = digitalRead(_wiresPin[iwire]);
          if (!_wasUsedWires[iwire] && iwire == _correctWire && buttonState == HIGH) {
            _wasUsedWires[iwire] = true;
            wiresOk();
          }
          else if (!_wasUsedWires[iwire] && iwire != _correctWire && buttonState == HIGH) {
            _wasUsedWires[iwire] = true;
            wiresBad();
          }
        }
      }

    private:
    
      void resetWires() {
        for (int iwire = 0 ; iwire < _wireNb ; ++iwire) {
          _wasUsedWires[iwire] = false;
        }
      }
    
      void wiresOk() {
        //Serial.println("Bombe désamorcée !");
        //_sevSegClock.setNumber(8888, 0);
        _cs->stop();
      }
     
      void wiresBad() {
        _es->addError();
      }
    
    }; // class SimpleWires
      
  /************/
  /*** MAIN ***/
  /************/
  
  CounterState counterState;
  ErrorState errorLed;
  ClockState clockState;
  SimpleWires wires;

  public:
    DemoCounterAndWires() : 
      counterState(),
      clockState(),
      errorLed(clockState),
      wires(clockState, errorLed) { }
    
    void setup() {
      setupPortExpander();

      counterState.init();
      clockState.init();
      errorLed.init();
      wires.init();
      
      counterState.setState(CounterState::State::Running);
      clockState.start();
    }
  
    void loop() {
      clockState.displayClock();
      wires.checkWires();
      //readInput();
    }

  }; // class DemoCounterAndWires

} // namespace PortExpander


/*************************************************/

PortExpander::DemoCounterAndWires counterAndWires;

void setup() {
  //delay(1*1000);
  counterAndWires.setup();
}

void loop() {
  counterAndWires.loop();
}



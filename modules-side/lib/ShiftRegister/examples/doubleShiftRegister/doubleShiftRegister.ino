#include <ShiftRegister.h>

/**
 * This example show how to use ShiftRegister library.
 * It use 2 SN74HC595 shift register in serie connected with :
 *  - pin 9, 10 and 11 of the Atmega328
 *  - leds and 330 Omh resistor on the shift registor output
 *
 * See : https://www.arduino.cc/en/Tutorial/ShiftOut example 2 for more details
 * Note that you can only call pinMode(), digitalWrite() and digitalRead() on virtual pins inside the ShiftRegisterHelper namespace.
 **/


namespace ShiftRegisterHelper {
  
  void setup() {
    uint8_t ds_pin   = 9;
    uint8_t stcp_pin = 10;
    uint8_t shcp_pin = 11;
    int sr_num = 2;

    // We tell the program than we have a shift register connected to pin 9, 10 and 11.
    ShiftRegister::setupShiftRegister(ds_pin, shcp_pin, stcp_pin, sr_num);    
    // We can now use virtual pin from ShiftRegister::emulatedPinMin() to ShiftRegister::emulatedPinMax() (excluded)
  }

  void loop() {
    static int counter = 0;
    int lenght = ShiftRegister::emulatedPinMax() - ShiftRegister::emulatedPinMin();

    // digitalWrite is called on a virtual pin.
    // this mean that is not a real pin of the Atmega but a pin of your shift register
    digitalWrite(ShiftRegister::emulatedPinMin() + (counter%lenght), (counter % (lenght*2) < lenght) ? HIGH : LOW);
    counter++;
    delay(150);
  }

} // namespace ShiftRegisterHelper

void setup() {
  ShiftRegisterHelper::setup();
}

void loop() {
  ShiftRegisterHelper::loop();
}


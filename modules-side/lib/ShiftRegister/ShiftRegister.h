/**
 * ShiftRegister.hpp - Library to help with use of shift registers
 * Created by Lux Benjamin, june 22, 2017
 *
 **/

#pragma once
// #ifndef SHIFT_REGISTER_SN74HC595_HELPER
// #define SHIFT_REGISTER_SN74HC595_HELPER

// #include <assert.h>
// #include <vector>

#include "Arduino.h"


/**
 * Help with Shift Regisiter (SR) SN74HC595 as an output
 * After initialisation, SR uses become transparent
 * It emulates virtual pin which are grether than 20 (20 pin for Arduino Uno)
 *
 * This is not appropriate for particular use of SR, like shifting value one by one.
 * For exemple shifting a light led.
 * 
 * In Arduino core function pin type is uint8_t. uint8_t (0 to 255).
 * So if we want to keep arduino prototypes we can't use more than 255 pin.
 *
 *
 * from SN74HC595 Data sheet 
 * at https://assets.nexperia.com/documents/data-sheet/74HC_HCT595.pdf
 *       ____ ____
 *      |    U    |
 * Q1   |1      16| Vcc*   supply voltage (+5 V)
 * Q2   |2      15| Q0     parallel data output 0
 * Q3   |3      14| DS*    serial data input
 * Q4   |4      13| OE**   output enable input (active LOW)
 * Q5   |5      12| STCP*  storage register clock input
 * Q6   |6      11| SHCP*  shift register clock input
 * Q7   |7      10| MR**   master reset (active LOW)
 * GND* |8       9| Q7S    serial data output
 *      |_________|
 * 
 * Q0 to Q8  parallel data output 0
 * GND       ground (0 V)
 *
 * *: this pin have to be connect to the Arduino
 * **: you may want to connect these pin to :
 *   MR : connect to HIGH
 *   OE : connect to LOW
 **/



/**
 * Arduino pin function are :
 *
 * // Digital I/O
 * void pinMode(uint8_t pin, uint8_t mode);
 * void digitalWrite(uint8_t pin, uint8_t val);
 * int digitalRead(uint8_t pin);
 % *
 * // Analog I/O
 * void analogReference(uint8_t mode);
 * int analogRead(uint8_t pin);
 * void analogWrite(uint8_t pin, int val); // - PWM
 *
 * // Due & Zero only
 * analogReadResolution()
 * analogWriteResolution()
 *
 * Here we wrap only the 3 first
 **/

/**
 * Usage :
 * decalre your physical distribution with this function :
 * ShiftRegister::ShiftRegisterBatch::setupShiftRegister(uint8_t ds_pin, uint8_t shcp_pin, uint8_t stcp_pin, int sr_num = 1)
 *
 * then you can use these functions as there is more pin :
 *
 * void vPinMode(uint8_t pin, uint8_t mode);
 * void vDigitalWrite(uint8_t pin, uint8_t val);
 * int  vDigitalRead(uint8_t pin);
 *
 * Virtual pin count starts at ShiftRegister::emulated_pin_min and end at emulated_pin_max (excluded).
 * Numbering depends on how you initialize your ShiftRegisters.
 **/

/**
 * Todo :
 * Documentation, clear code, comments and library.
 **/



namespace ShiftRegister {

  // Atmega328 have like 28 pin (?)
  const uint8_t sr_vpin_start = 40;
  
  constexpr uint8_t vpin(const uint8_t pin) {
    return pin + sr_vpin_start;
  }

  void setupShiftRegister(uint8_t ds_pin, uint8_t shcp_pin, uint8_t stcp_pin, int sr_num = 1);
  
  void pinMode(uint8_t pin, uint8_t mode);
  void digitalWrite(uint8_t pin, uint8_t val);
  int  digitalRead(uint8_t pin);
  
  /**
   * class ShiftRegisterBatch
   *
   * Only implemented as an output (for now)
   **/
  
  class ShiftRegisterBatch {
  public: // todo switch sh and st parameter to match physical orientation
    static const uint8_t& emulatedPinMin();
    static const uint8_t& emulatedPinMax();
    static const uint8_t& outPinNbBySR();
    
    friend void pinMode(uint8_t pin, uint8_t mode);
    friend void digitalWrite(uint8_t pin, uint8_t val);
    friend int  digitalRead(uint8_t pin);
    friend void setupShiftRegister(uint8_t ds_pin, uint8_t shcp_pin, uint8_t stcp_pin, int sr_num);
    
  private:
    // max pin + 1 on the Arduino Uno
    // min pin which can be emulated
    static const uint8_t _emulated_pin_min; // virtual pins start at emulated_pin_min, fix on a run
    static uint8_t _emulated_pin_max; // virtual pins ends  at emulated_pin_max-1, change in a run
    static const uint8_t _out_pin_nb_by_sr;

    uint8_t  _mode;
    uint8_t  _pin_nb;
    uint8_t  _first_pin;
    uint8_t  _last_pin;

    boolean* _digital_values_ptr = NULL;

    uint8_t  _ds_pin;    // 14: data  pin
    uint8_t  _shcp_pin;  // 11: clock pin
    uint8_t  _stcp_pin;  // 12: latch pin

    // Vector of all SR declared
    // static std::vector<ShiftRegisterBatch*> shiftRegisterPtrVect;
    static ShiftRegisterBatch* _shiftRegisterPtrVect[50]; // todo : not more than 50 SR ...
    static int _srPtrVectCmpt;
    
    ShiftRegisterBatch(uint8_t ds_pin, uint8_t shcp_pin, uint8_t stcp_pin, int sr_num,
		  uint8_t m = OUTPUT);
    ~ShiftRegisterBatch();
       
    static void setupShiftRegister(uint8_t ds_pin, uint8_t shcp_pin, uint8_t stcp_pin, int sr_num = 1);

    static ShiftRegisterBatch* _getShiftRegister(uint8_t pin);
    
    void _vDigitalWrite(uint8_t pin, uint8_t val);
    int  _vDigitalRead(uint8_t pin) const;
    boolean _containsPin(uint8_t pin) const;

    // a clock tick
    void _clock();
    // Shift data from internal buffer to output
    void _shiftBuffer();
    void _applyInternalBuffer();
    
  }; // class ShiftRegisterBatch
  

} // ShiftRegister





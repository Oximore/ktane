
#include "Arduino.h"
#include "ShiftRegister.h"

namespace ShiftRegister {
  
  // SN74HC595 have 8 output for each chip
  const uint8_t ShiftRegisterBatch::_out_pin_nb_by_sr = 8;

  ShiftRegisterBatch* ShiftRegisterBatch::_shiftRegisterPtrVect[50]; // todo : not more than 50 SR ...
  int ShiftRegisterBatch::_srPtrVectCmpt = 0;  
   
  const uint8_t ShiftRegisterBatch::_emulated_pin_min = sr_vpin_start;
  uint8_t ShiftRegisterBatch::_emulated_pin_max = sr_vpin_start;


  ShiftRegisterBatch::ShiftRegisterBatch(uint8_t ds_pin, uint8_t shcp_pin, uint8_t stcp_pin, int sr_num,
			       uint8_t m) {

    uint8_t first_pin = _emulated_pin_min;
    // if ( !shiftRegisterPtrVect.empty() ) {
    // 	first_pin = shiftRegisterPtrVect.last()->_last_pin+1;
    // }
      
    if (_srPtrVectCmpt != 0) {
      first_pin = _shiftRegisterPtrVect[_srPtrVectCmpt-1]->_last_pin + 1;
    }

    // assert(m == mode::output);
    // assert(m == OUTPUT);
      
    // Initialise SR input pin
    uint8_t pin_mode =  m;
    pinMode(ds_pin,   pin_mode);
    pinMode(shcp_pin, pin_mode);
    pinMode(stcp_pin, pin_mode);
      
    // Initialise internal datas
    _mode = m;
    _ds_pin   = ds_pin;
    _shcp_pin = shcp_pin;
    _stcp_pin = stcp_pin;

    // todo suppress this if (?)
    if (first_pin >= _emulated_pin_min) {
      _pin_nb = _out_pin_nb_by_sr * sr_num;
      _first_pin = first_pin;
      _last_pin = _first_pin + _pin_nb - 1;
      _emulated_pin_max += _pin_nb;
      _digital_values_ptr = new boolean[_pin_nb];

      // initialise values to LOW
      for (uint8_t ivpin = 0 ; ivpin <= _pin_nb ; ++ivpin ) {
	_digital_values_ptr[ivpin] = LOW;
      }
    }
    
  }

  ShiftRegisterBatch::~ShiftRegisterBatch() {
    if (_digital_values_ptr != NULL) {
      delete[] _digital_values_ptr;
      _digital_values_ptr = NULL;
    }
  }

       
    
    
  void ShiftRegisterBatch::setupShiftRegister(uint8_t ds_pin, uint8_t shcp_pin, uint8_t stcp_pin, int sr_num) {
    // shiftRegisterPtrVect.push_back(new ShiftRegisterBatch(ds_pin, shcp_pin, stcp_pin, sr_num));    
    _shiftRegisterPtrVect[_srPtrVectCmpt++] = new ShiftRegisterBatch(ds_pin, shcp_pin, stcp_pin, sr_num);    
  }

  void setupShiftRegister(uint8_t ds_pin, uint8_t shcp_pin, uint8_t stcp_pin, int sr_num) {
    ShiftRegisterBatch::setupShiftRegister(ds_pin, shcp_pin, stcp_pin, sr_num);
  }

  
  ShiftRegisterBatch* ShiftRegisterBatch::_getShiftRegister(uint8_t pin) {
    for (int isrPtr = 0 ; isrPtr < _srPtrVectCmpt ; ++isrPtr) {
      if ( _shiftRegisterPtrVect[isrPtr]->_containsPin(pin) ) {
	return _shiftRegisterPtrVect[isrPtr];
      }
    }
            
    return NULL;
  }
    

  void ShiftRegisterBatch::_vDigitalWrite(uint8_t pin, uint8_t val) {
    if (_vDigitalRead(pin) != val) {
      // Update internal values
      _digital_values_ptr[pin - _first_pin] = val;
      // Display this values
      _applyInternalBuffer(); 

    }
  }

  int ShiftRegisterBatch::_vDigitalRead(uint8_t pin) const{
    return _digital_values_ptr[pin-_first_pin];
  }

    
  boolean ShiftRegisterBatch::_containsPin(uint8_t pin) const {
    return _first_pin <= pin && pin <= _last_pin;
  }

  const uint8_t& ShiftRegisterBatch::emulatedPinMin() {
    return _emulated_pin_min;
  }
    
  const uint8_t& ShiftRegisterBatch::emulatedPinMax() {
    return _emulated_pin_max;
  }

  const uint8_t& ShiftRegisterBatch::outPinNbBySR() {
    return _out_pin_nb_by_sr;
  }
    

  // todo suppress Helper::

  // a clock tick
  void ShiftRegisterBatch::_clock() {
    digitalWrite(_shcp_pin, HIGH);
    delayMicroseconds(30); // Todo : find somthing else
    digitalWrite(_shcp_pin, LOW);
  }

  // Shift data from internal buffer to output
  void ShiftRegisterBatch::_shiftBuffer() {
    digitalWrite(_stcp_pin, HIGH);
    delayMicroseconds(30); // Todo : find somthing else
    digitalWrite(_stcp_pin, LOW);
  }

  void ShiftRegisterBatch::_applyInternalBuffer() {
    for ( int ibit = _pin_nb-1 ; ibit >= 0 ; --ibit ) {
      digitalWrite(_ds_pin, _digital_values_ptr[ibit]);
      _clock();
    }
    _shiftBuffer();
  }



  
  /**
   * Bad naming
   * todo : static ?
   **/
  inline boolean isEmulated(uint8_t pin) {
    return pin >= ShiftRegisterBatch::emulatedPinMin();
  }


  /**
   * Arduino wrapping function
   **/
  
  // Digital I/O
  
  void pinMode(uint8_t pin, uint8_t mode) {
    if ( isEmulated(pin) ) {
      ShiftRegisterBatch* srPtr = ShiftRegisterBatch::_getShiftRegister(pin);
      // check
      // assert(srPtr->_mode == mode);
    }
    else {
      // call Arduino pinMode
      ::pinMode(pin,mode);
    }
  }

  void digitalWrite(uint8_t pin, uint8_t val) {
    if ( isEmulated(pin) ) {
      ShiftRegisterBatch::_getShiftRegister(pin)->_vDigitalWrite(pin, val);
    }
    else {
      // call Arduino digitalWrite
      ::digitalWrite(pin, val);
    }
  }

  int digitalRead(uint8_t pin) {
    if ( isEmulated(pin) ) {
      return ShiftRegisterBatch::_getShiftRegister(pin)->_vDigitalRead(pin);
    }
    else {
      // call Arduino digitalRead
      return ::digitalRead(pin);
    }
  }
  
} // namespace ShiftRegister

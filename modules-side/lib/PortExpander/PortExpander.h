/**
 * PortExpander.h - Library to help with use of MCP23017 I2C port expander
 * Created by Lux Benjamin, dec 13, 2017
 **/


#pragma once

#include "Arduino.h"
#include <Wire.h>
#include <Adafruit_MCP23017.h>

/**
 * Help with Port Expander (PE) MCP2301
 * After initialisation, PE uses become transparent
 * It emulates virtual pin which are grether than pe_vpin_start (40 pin for Arduino Uno)
 *
 * You should connect your port expander like :
 * https://github.com/adafruit/Adafruit-MCP23017-Arduino-Library
 *
 **/



/**
 * Arduino pin function are :
 *
 * // Digital I/O
 * void pinMode(uint8_t pin, uint8_t mode);
 * void digitalWrite(uint8_t pin, uint8_t val);
 * int digitalRead(uint8_t pin);
 % *
 * // Analog I/O
 * void analogReference(uint8_t mode);
 * int analogRead(uint8_t pin);
 * void analogWrite(uint8_t pin, int val); // - PWM
 *
 * // Due & Zero only
 * analogReadResolution()
 * analogWriteResolution()
 *
 * Here we wrap only the 3 first
 **/

/**
 * Usage :
 * decalre your physical distribution with this function :
 * PortExpander::setupPortExpander(uint8_t address = 0)
 *
 * then you can use these functions as there is more pin :
 *
 * void pinMode(uint8_t pin, uint8_t mode);
 * void digitalWrite(uint8_t pin, uint8_t val);
 * int  digitalRead(uint8_t pin);
 * 
 * void pullUp(uint8_t pin, uint8_t val)
 * this one is equivalent to pinMode(pin, INPUT) and digitalWrite(pin, HIGH) on normal pin
 *
 * You can get the n°virtual pin with vpin(n)
 * 
 * Virtual pin count starts at PortExpander::MCP23017PortExpander::emulatedPinMin() and end at emulatedPinMax() (excluded).
 * Numbering depends on how you initialize your port expander.
 **/

/**
 * Todo :
 * Documentation, clear code, comments and library.
 **/


namespace PortExpander {

  const uint8_t pe_vpin_start = 40;
  
  constexpr uint8_t vpin(const uint8_t pin) {
    return pin + pe_vpin_start;
  }

  void setupPortExpander(int address = 0);
  
  void pinMode(uint8_t pin, uint8_t mode);
  void digitalWrite(uint8_t pin, uint8_t val);
  int  digitalRead(uint8_t pin);
  void pullUp(uint8_t pin, uint8_t val);

  /**
   * class MCP23017PortExpander
   **/

  class MCP23017PortExpander {
  public:
    static void setupPortExpander(int address);

    static uint8_t emulatedPinMin();
    static uint8_t emulatedPinMax();

    friend void pinMode(uint8_t pin, uint8_t mode);
    friend void digitalWrite(uint8_t pin, uint8_t val);
    friend int  digitalRead(uint8_t pin);
    friend void pullUp(uint8_t pin, uint8_t val);
    
  private:
    // max pin + 1 on the Arduino Uno
    // min pin which can be emulated
    static uint8_t _emulated_pin_min; // virtual pins start at emulated_pin_min, fix on a run
    static uint8_t _emulated_pin_max; // virtual pins ends  at emulated_pin_max-1, change in a run

    const static uint8_t _pin_nb; //out_pin_nb_by_pe;
    const static uint8_t _scl_pin;
    const static uint8_t _sda_pin;

    Adafruit_MCP23017 _mcp;
    uint8_t  _first_pin;
    uint8_t  _last_pin;

    // Vector of all PE declared
    static MCP23017PortExpander* _portExpanderPtrVect[50]; // todo : not more than 50 PE ...
    static int _pePtrVectCmpt;

    MCP23017PortExpander(int address);
    ~MCP23017PortExpander();

    static MCP23017PortExpander* _getMCP23017PortExpander(uint8_t pin);

    void _vPinMode(uint8_t pin, uint8_t mode);
    void _vDigitalWrite(uint8_t pin, uint8_t val);
    int  _vDigitalRead(uint8_t pin);
    void _vPullUp(uint8_t pin, uint8_t val);
    boolean _containsPin(uint8_t pin) const;

  }; // class MCP23017PortExpander


} // PortExpander

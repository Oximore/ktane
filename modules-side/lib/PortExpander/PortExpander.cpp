#include "Arduino.h"
#include "PortExpander.h"

namespace PortExpander {

  // MCP23017 have 16 output for each chip
  const uint8_t MCP23017PortExpander::_pin_nb = 16; //out_pin_nb_by_pe = 16;

  const uint8_t MCP23017PortExpander::_scl_pin = 12; // 12: I2C clock
  const uint8_t MCP23017PortExpander::_sda_pin = 13; // 13: I2C data

  MCP23017PortExpander* MCP23017PortExpander::_portExpanderPtrVect[50];
  // todo : max with 3 bit address ...
  int MCP23017PortExpander::_pePtrVectCmpt = 0;

  // Atmega328 have like 20 pin (?)
  uint8_t MCP23017PortExpander::_emulated_pin_min = pe_vpin_start;
  uint8_t MCP23017PortExpander::_emulated_pin_max = pe_vpin_start;


  MCP23017PortExpander::MCP23017PortExpander(int address) {
    _mcp.begin(address);

    uint8_t first_pin = _emulated_pin_max;

    if (_pePtrVectCmpt != 0) {
      first_pin = _portExpanderPtrVect[_pePtrVectCmpt-1]->_last_pin + 1;
    }

    if (first_pin >= pe_vpin_start) {
      _first_pin = first_pin;
      _last_pin = _first_pin + _pin_nb - 1;
      _emulated_pin_max += _pin_nb;
    }

  }

  MCP23017PortExpander::~MCP23017PortExpander() {
  }


  void MCP23017PortExpander::setupPortExpander(int address) {
    _portExpanderPtrVect[_pePtrVectCmpt++] = new MCP23017PortExpander(address);
  }

  MCP23017PortExpander* MCP23017PortExpander::_getMCP23017PortExpander(uint8_t pin) {
    for (int ipePtr = 0 ; ipePtr < _pePtrVectCmpt ; ++ipePtr) {
      if ( _portExpanderPtrVect[ipePtr]->_containsPin(pin) ) {
	return _portExpanderPtrVect[ipePtr];
      }
    }

    return NULL;
  }


  void MCP23017PortExpander::_vPinMode(uint8_t pin, uint8_t mode) {
    _mcp.pinMode(pin-_first_pin, mode);
  }

  void MCP23017PortExpander::_vDigitalWrite(uint8_t pin, uint8_t val) {
    _mcp.digitalWrite(pin-_first_pin, val);
  }

  int MCP23017PortExpander::_vDigitalRead(uint8_t pin) {
    return _mcp.digitalRead(pin-_first_pin);
  }

  void MCP23017PortExpander::_vPullUp(uint8_t pin, uint8_t val) {
    return _mcp.pullUp(pin-_first_pin, val);
  }


  
  boolean MCP23017PortExpander::_containsPin(uint8_t pin) const {
    return _first_pin <= pin && pin <= _last_pin;
  }

  uint8_t MCP23017PortExpander::emulatedPinMin() {
    return _emulated_pin_min;
  }

  uint8_t MCP23017PortExpander::emulatedPinMax() {
    return _emulated_pin_max;
  }


  /**
   * Bad naming
   * todo : static ?
   **/
  inline boolean isEmulated(uint8_t pin) {
    return pin >= MCP23017PortExpander::emulatedPinMin();
  }


  void setupPortExpander(int address) {
    MCP23017PortExpander::setupPortExpander(address);
  }

  
  /**
   * Arduino wrapping function
   **/

  // Digital I/O

  void pinMode(uint8_t pin, uint8_t mode) {
    if ( isEmulated(pin) ) {
      MCP23017PortExpander::_getMCP23017PortExpander(pin)->_vPinMode(pin, mode);
    }
    else {
      // call Arduino pinMode
      ::pinMode(pin,mode);
    }
  }

  void digitalWrite(uint8_t pin, uint8_t val) {
    if ( isEmulated(pin) ) {
      MCP23017PortExpander::_getMCP23017PortExpander(pin)->_vDigitalWrite(pin, val);
    }
    else {
      // call Arduino digitalWrite
      ::digitalWrite(pin, val);
    }
  }

  int digitalRead(uint8_t pin) {
    if ( isEmulated(pin) ) {
      return MCP23017PortExpander::_getMCP23017PortExpander(pin)->_vDigitalRead(pin);
    }
    else {
      // call Arduino digitalRead
      return ::digitalRead(pin);
    }
  }

  void pullUp(uint8_t pin, uint8_t val) {
    if ( isEmulated(pin) ) {
      MCP23017PortExpander::_getMCP23017PortExpander(pin)->_vPullUp(pin, val);
    }
    else {
      // hummmm
      // digitalWrite(pin, val); // and check that is INPUT ?
    }
  }

} // namespace PortExpander


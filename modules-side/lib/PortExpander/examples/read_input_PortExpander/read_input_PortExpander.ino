#include <PortExpander.h>

/**
 * This example show how to use ShiftRegister library
 * It use a single SN74HC595 shift register connected with :
 *  - pin 9, 10 and 11 of the Atmega328
 *  - leds and 330 Omh resistor on the shift registor output
 *
 * Note that you can only call pinMode(), digitalWrite() and digitalRead() on virtual pins inside the ShiftRegisterHelper namespace.
 **/


namespace PortExpander {
   
  int pin_wire, pin_led, pin_debug;
  
  void setup() {
    setupPortExpander();   

    pin_debug = LED_BUILTIN;
    pin_led  = vpin(0);
    pin_wire = vpin(8);

    pinMode(pin_debug, OUTPUT);
    pinMode(pin_led,   OUTPUT);
    
    pinMode(pin_wire, INPUT);
    pullUp(pin_wire, HIGH);  // turn on a 100K pullup internally
  }
  
  void loop() {
    digitalWrite(pin_debug, digitalRead(pin_wire));
    digitalWrite(pin_led,   digitalRead(pin_wire));
  }

} // namespace PortExpander

void setup() {
  PortExpander::setup();
}

void loop() {
  PortExpander::loop();
}


#include <PortExpander.h>

/**
 * This example show how to use ShiftRegister library
 * It use a single SN74HC595 shift register connected with :
 *  - pin 9, 10 and 11 of the Atmega328
 *  - leds and 330 Omh resistor on the shift registor output
 *
 * Note that you can only call pinMode(), digitalWrite() and digitalRead() on virtual pins inside the ShiftRegisterHelper namespace.
 **/


namespace PortExpander {
   
  void setup() {
    setupPortExpander();    
    // We can now use virtual pin from ShiftRegister::emulatedPinMin() to ShiftRegister::emulatedPinMax() (excluded)
    for (int ipin = 0; ipin < 16 ; ++ipin) {
      pinMode(vpin(ipin), OUTPUT);
    }
  }

  int counter = 0;
  void loop() {
    digitalWrite(vpin(counter%16), HIGH);
    delay(500);
    digitalWrite(vpin(counter%16), LOW);
    ++counter;
  }

} // namespace PortExpander

void setup() {
  PortExpander::setup();
}

void loop() {
  PortExpander::loop();
}


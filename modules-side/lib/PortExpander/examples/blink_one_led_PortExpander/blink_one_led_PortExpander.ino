#include <PortExpander.h>

/**
 * This example show how to use ShiftRegister library
 * It use a single SN74HC595 shift register connected with :
 *  - pin 9, 10 and 11 of the Atmega328
 *  - leds and 330 Omh resistor on the shift registor output
 *
 * Note that you can only call pinMode(), digitalWrite() and digitalRead() on virtual pins inside the ShiftRegisterHelper namespace.
 **/


namespace PortExpander {
   
  int pin = 0;
  int debug_pin = LED_BUILTIN;
  
  void setup() {
    setupPortExpander();    
    // We can now use virtual pin from ShiftRegister::emulatedPinMin() to ShiftRegister::emulatedPinMax() (excluded)
    pinMode(vpin(pin), OUTPUT);
    pinMode(debug_pin, OUTPUT);
  }

  
  void loop() {
    digitalWrite(vpin(pin), HIGH);
    digitalWrite(debug_pin, HIGH);
    delay(500);
  
    digitalWrite(vpin(pin), LOW);
    digitalWrite(debug_pin, LOW);
    delay(500);
  }

} // namespace PortExpander

void setup() {
  PortExpander::setup();
}

void loop() {
  PortExpander::loop();
}

